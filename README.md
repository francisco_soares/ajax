# Curos de Ajax

**Aula 426 - Introdução ao Ajax**
### **AJAX**
Acrônimo de **A**synchronous **J**avaScript **A**nd **X**ML<br><br>

Uma metodologia de programação que possivilita a comunicação assíncrona entre front-end r back-end de aplicações Web.<br><br>

Inicialmente disponibilizado na versão 4.0 do Internet Explorer<br><br>

Potencializou a criação do conceito de web 2.0

==========================================


**Aula 427 - O que são requisições sincronas e assíncronas**<br><br>

**Síncronas** - Envia uma requisição por vez

**Assíncronas** - Envia várias requisições simultâneamente.

